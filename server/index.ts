import fastify, { FastifyInstance } from "fastify";
import fastifyCors from "fastify-cors";
import fastifyStatic from "fastify-static";
import { IncomingMessage, Server, ServerResponse } from "http";
import * as path from "path";
import { connect as DbConnect } from "./db";
import routes from "./routes";

export const server: FastifyInstance<Server, IncomingMessage, ServerResponse> = fastify();

server.register(fastifyCors);
server.register(fastifyStatic, {
  root: path.join(__dirname, "frontend"),
});
routes(server);

server.listen(3000, async (err, address) => {
  await DbConnect();
  if (err) {
    console.error(err);
    process.exit(0);
  }
  console.log(`⚡️ Server listening at ${address} ⚡️`);
});
