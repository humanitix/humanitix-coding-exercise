import { getModelForClass, prop } from "@typegoose/typegoose";
import { Units } from "../@types/requests";
import { BaseModel } from "./BaseModel";

export default class Widget extends BaseModel {
  @prop()
  slug!: string;

  @prop()
  title!: string;

  @prop({ enum: Units })
  units!: Units;

  @prop()
  windSwitch!: boolean;
}

export const WidgetModel = getModelForClass(Widget);
