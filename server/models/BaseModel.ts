import { mongoose, prop } from "@typegoose/typegoose";
import { TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

export class BaseModel extends TimeStamps {
  @prop({
    default: function () {
      return new mongoose.Types.ObjectId();
    },
  })
  _id?: mongoose.Types.ObjectId;
}
