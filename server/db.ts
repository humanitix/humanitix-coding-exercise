import { mongoose } from "@typegoose/typegoose";
import fetch from "node-fetch";

const getUri = async () => {
  if (process.env.DB_URI) return process.env.DB_URI;

  const dbServerEndpoint = `${process.env.DB_SERVER}/getUri`;
  try {
    return (await (await fetch(dbServerEndpoint)).json()).uri;
  } catch (err) {
    console.error(`Failed to fetch DB uri from ${dbServerEndpoint}`);
  }
  return;
};

export const connect = async (): Promise<void> => {
  try {
    const uri = await getUri();
    if (!uri) {
      console.error("No db uri found. Service going unhealthy");
      process.exit(0);
    }
    console.info(`Mongoose.connecting to ${uri}`);
    await mongoose.connect(await getUri(), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });

    mongoose.connection.once("error", function (err) {
      console.error("Error on mongoDb connection. Service going unhealthy", err);
      process.exit(0);
    });
    mongoose.connection.once("disconnected", function () {
      console.error("Connection disconnected. Attempting reconnect in 2s...");
      setTimeout(connect, 2000);
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
};
