export interface GetWidgetParams {
  slug?: string;
}

export enum Units {
  metric = "metric",
  imperial = "imperial",
}

export interface SaveBody {
  title: string;
  units: Units;
  windSwitch: boolean;
}
