import { jest, describe, it, expect } from "@jest/globals";
import { Units } from "../@types/requests";
import * as controller from "../controller";
import Widget from "../models/Widget";
import { FastifyReply } from "fastify";

const leanMock = jest.fn();
const findOneAndUpdateMock = jest.fn();
const replyMock = ({
  type: jest.fn(),
} as unknown) as FastifyReply;
jest.mock("../models/Widget", () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const originalModule: any = jest.requireActual("../models/Widget");
  return {
    __esModule: true,
    ...originalModule,
    WidgetModel: {
      find: () => ({
        lean: () => leanMock(),
      }),
      findOne: () => ({
        lean: () => leanMock(),
      }),
      findOneAndUpdate: () => findOneAndUpdateMock(),
    },
  };
});

jest.mock("fs", () => {
  return {
    readFile: (path: string, cb: (err: null, data: string) => void) => {
      setTimeout(() => {
        cb(null, "window.__WWW");
      }, 0);
    },
  };
});

describe("Controller", () => {
  describe("getWidgets", () => {
    it("returns 404 when no widgets found", async () => {
      leanMock.mockReturnValueOnce(null);
      await expect(controller.getWidgets()).rejects.toEqual({ statusCode: 404, message: "Not Found" });
    });

    it("returns list of widgets", async () => {
      const widgets = [new Widget(), new Widget()];
      leanMock.mockReturnValueOnce(widgets);
      await expect(controller.getWidgets()).resolves.toEqual(widgets);
    });
  });

  describe("saveWidget", () => {
    it("returns new slug", async () => {
      const widget = new Widget();
      widget.title = "All the things";
      widget.units = Units.imperial;
      widget.windSwitch = true;

      const { slug } = await controller.saveWidget({ body: widget });
      expect(findOneAndUpdateMock).toHaveBeenCalled();
      expect(slug).toEqual("all-the-things");
    });
  });

  describe("getWidgetPage", () => {
    it("returns 404 when no widget found", async () => {
      leanMock.mockReturnValueOnce(null);
      await expect(controller.getWidgetPage({ params: { slug: "none-of-the-things" } }, replyMock)).rejects.toEqual({
        statusCode: 404,
        message: "Not Found",
      });
    });
    it("returns page html string with widget data inlined", async () => {
      const widget = new Widget();
      widget.title = "All the things";
      widget.units = Units.imperial;
      widget.windSwitch = true;
      leanMock.mockReturnValueOnce(widget);

      const index = await controller.getWidgetPage({ params: { slug: "all-the-things" } }, replyMock);
      expect(index).toContain(`\"title\":\"All the things\"`);
    });
  });
});
