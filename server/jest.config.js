const config = {
  rootDir: "./",
  moduleFileExtensions: ["ts", "js", "json"],
  preset: "ts-jest",
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.json",
    },
  },
  testMatch: ["<rootDir>/__tests__/*.spec.ts"],
  testPathIgnorePatterns: ["build/"],
  clearMocks: true,
  resetMocks: true,
  testEnvironment: "node",
};

module.exports = config;
