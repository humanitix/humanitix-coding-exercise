import { FastifyInstance } from "fastify";
import { GetWidgetParams, SaveBody } from "./@types/requests";
import { getWidgetPage, getWidgets, saveWidget } from "./controller";

export default (server: FastifyInstance): void => {
  server.get("/widgets", {}, getWidgets);

  server.get<{
    Params: GetWidgetParams;
  }>("/widgets/:slug", {}, getWidgetPage);

  server.put<{
    Body: SaveBody;
  }>("/widgets", {}, saveWidget);
};
