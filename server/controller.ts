import { FastifyReply } from "fastify";
import { readFile } from "fs";
import * as path from "path";
import slugify from "slugify";
import { GetWidgetParams, SaveBody } from "./@types/requests";
import Widget, { WidgetModel } from "./models/Widget";

export const getWidgetPage = async (request: { params: GetWidgetParams }, response: FastifyReply): Promise<string> => {
  const slug = request.params.slug;
  const widget = await WidgetModel.findOne({ slug: slug }).lean();
  if (!widget) throw { statusCode: 404, message: "Not Found" };
  const index: string = await new Promise((resolve) => {
    readFile(path.join(__dirname, "frontend/index.html"), (err, data) => {
      resolve(data.toString("utf-8"));
    });
  });
  response.type("texts/html");
  return index.replace("window.__WWW", `window.__WWW=${JSON.stringify(widget)}`);
};

export const getWidgets = async (): Promise<Widget[]> => {
  const widgets = await WidgetModel.find().lean();
  if (!widgets) throw { statusCode: 404, message: "Not Found" };

  return widgets;
};

export const saveWidget = async (request: { body: SaveBody }): Promise<{ slug: string }> => {
  const slug = slugify(request.body.title, { lower: true });
  await WidgetModel.findOneAndUpdate({ slug: slug }, request.body, {
    upsert: true,
  });
  return { slug };
};
