import fastify, { FastifyInstance } from "fastify";
import { IncomingMessage, Server, ServerResponse } from "http";
import { MongoMemoryServer } from "mongodb-memory-server";

let connectionString: string | undefined = undefined;
export const start = async () => {
  try {
    const mongoServer = new MongoMemoryServer();
    await mongoServer.start();
    connectionString = mongoServer.getUri();
    console.log(`🔌 Database uri: 🔌`);
    console.log(connectionString);
  } catch (err) {
    console.error(err);
  }
};

const server: FastifyInstance<
  Server,
  IncomingMessage,
  ServerResponse
> = fastify();

server.get("/getUri", {}, async () => {
  return { uri: connectionString };
});

server.listen(3020, async (err, address) => {
  await start();
  if (err) {
    console.error(err);
    process.exit(0);
  }
  console.log(`⚡️ Database server listening at ${address} ⚡️`);
});
