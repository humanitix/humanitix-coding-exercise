import React from "react";
import ReactDOM from "react-dom";
import WeatherWidgetApp from "./App";
import Page from "./Page";

if (window.__WWW) {
  ReactDOM.render(<Page {...window.__WWW} />, document.getElementById("root"));
} else {
  ReactDOM.render(<WeatherWidgetApp />, document.getElementById("root"));
}
