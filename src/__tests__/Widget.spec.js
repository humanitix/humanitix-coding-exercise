import React from "react";
import "jest-dom/extend-expect";
import { render, cleanup } from "react-testing-library";
import Widget from "../containers/Widget";

describe("When rendered", () => {
  let container, getByText, props;

  beforeEach(() => {
    props = {
      city: "Sydney",
      dispTemp: "20",
      weather: {
        description: "test desc",
        icon: "10",
      },
      wind: "test wind",
      windSwitch: true,
    };
  });

  afterEach(cleanup);

  test("title is displayed if there is one", () => {
    ({ container, getByText } = render(
      <Widget {...props} title="Test title" />
    ));

    expect(container).toBeDefined();
    expect(getByText("Test title")).toBeDefined();
  });

  test("default text is displayed if there is no title", () => {
    ({ container, getByText } = render(<Widget {...props} title="" />));

    expect(container).toBeDefined();
    expect(getByText("Title of widget")).toBeDefined();
  });
});
