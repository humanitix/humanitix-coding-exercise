import React from "react";
import { render, cleanup, fireEvent } from "react-testing-library";
import WeatherWidget, {
  testDirection,
  getWindDirection,
  convertToFahrenheit,
  measurementChange,
  getLocation,
  fetchURL,
  fetchWeather,
  setWeatherData,
  processLocation,
  formChange,
} from "../App";

const navigator = {
  geolocation: {
    getCurrentPosition: (callback) => {
      return callback({
        coords: {
          latitude: -33.86882,
          longitude: 151.209296,
        },
      });
    },
  },
};

const apiResponse = {
  coord: {
    lon: 151.11,
    lat: -33.79,
  },
  weather: [
    {
      id: 803,
      main: "Clouds",
      description: "broken clouds",
      icon: "04n",
    },
  ],
  base: "stations",
  main: {
    temp: 21.48,
    pressure: 1011,
    humidity: 78,
    temp_min: 21,
    temp_max: 22,
  },
  visibility: 10000,
  wind: {
    speed: 7.2,
    deg: 20,
  },
  clouds: {
    all: 75,
  },
  dt: 1541325600,
  sys: {
    type: 1,
    id: 8233,
    message: 0.0056,
    country: "AU",
    sunrise: 1541271149,
    sunset: 1541319971,
  },
  id: 7839769,
  name: "Ryde",
  cod: 200,
};

test("testDirection", () => {
  const result = testDirection(60, "N");
  const result2 = testDirection(10, "N");
  expect(result).toBeFalsy();
  expect(result2).toBeTruthy();
});

test("getWindDirection", () => {
  const result = getWindDirection(60);
  const result2 = getWindDirection(350);
  expect(result).toEqual("ENE");
  expect(result2).toEqual("N");
});

test("convertToFahrenheit", () => {
  const result = convertToFahrenheit(20);
  const result2 = convertToFahrenheit(12);
  expect(result).toEqual(68);
  expect(result2).toEqual(54);
});

test("measurementChange", () => {
  const result = measurementChange("metric", 20);
  const result2 = measurementChange("imperial", 20);
  expect(result).toEqual(20);
  expect(result2).toEqual(68);
});

test("getLocation", () => {
  let cb = (loc) => ({
    lat: loc.lat,
    lon: loc.lon,
  });
  const result = getLocation(cb, navigator);
  expect(result.lat).toEqual(-33.86882);
  expect(result.lon).toEqual(151.209296);
});

test("fetchURL", () => {
  const loc = {
    lat: -33.86882,
    lon: 151.209296,
  };
  const result = fetchURL(loc);
  expect(result).toEqual(
    "https://api.openweathermap.org/data/2.5/weather?lat=-33.86882&lon=151.209296&APPID=d547cd544b44226d45c92f8436d2eda1&units=metric"
  );
});

test("fetchWeather", () => {
  const loc = {
    lat: -33.86882,
    lon: 151.209296,
  };
  const succ = () => "success";
  const fail = () => "failure";

  const result = fetchWeather(loc, succ, fail);
  expect(typeof result).toBe("object");
});

test("setWeatherData", () => {
  const result = setWeatherData(apiResponse, (x) => x);

  const expected = {
    city: "Ryde",
    dispTemp: 21,
    loading: false,
    temp: 21,
    weather: {
      id: 803,
      main: "Clouds",
      description: "broken clouds",
      icon: "04n",
    },
    wind: "NNE 7.2km/h",
  };

  const result2 = setWeatherData(
    {
      cod: 404,
    },
    (x) => x
  );

  const expected2 = {
    error: true,
    loading: false,
  };

  expect(result).toEqual(expected);
  expect(result2).toEqual(expected2);
});

test("proccessLocation", () => {
  const result = processLocation(
    {
      lat: -33.86882,
      lon: 151.209296,
    },
    (x) => x
  );

  expect(typeof result).toBe("object");

  const result2 = processLocation(false, (x) => x);

  expect(result2).toEqual({
    error: true,
    loading: false,
  });
});

test("formChange", () => {
  const _this = {
    state: {
      dispTemp: measurementChange(false, 20),
      temp: 20,
    },
    setState: (x) => {
      const oldState = _this.state;
      return (_this.state = {
        ...oldState,
        ...x(),
      });
    },
  };
  formChange("Melbourne", "city", _this);
  expect(_this.state.city).toEqual("Melbourne");

  formChange(false, "celsius", _this);
  expect(_this.state).toEqual({
    celsius: false,
    city: "Melbourne",
    dispTemp: 68,
    temp: 20,
  });
});

describe("On initial load", () => {
  let container, getByLabelText;

  beforeEach(() => {
    ({ container, getByLabelText } = render(<WeatherWidget />));
  });

  afterEach(cleanup);

  test("weather widget gets rendered correctly", () => {
    render(<WeatherWidget />);
    expect(container).toBeTruthy();
  });

  test("formChange works as expected", () => {
    const titleInput = getByLabelText("Title");
    fireEvent.change(titleInput, {
      target: { value: "New title" },
    });
    expect(titleInput.value.length).toEqual(9);
  });
});
