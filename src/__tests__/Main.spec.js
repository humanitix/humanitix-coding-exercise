import React from "react";
import "jest-dom/extend-expect";
import { render, cleanup } from "react-testing-library";
import Main from "../containers/Main";

describe("On data error", () => {
  let container, getByText, props;

  beforeEach(() => {
    props = {
      celsius: true,
      city: "Sydney",
      dispTemp: "20",
      error: true,
      formChange: () => false,
      loading: false,
      title: "New title",
      weather: {
        description: "test desc",
        icon: "10",
      },
      wind: "test wind",
      windSwitch: true,
    };

    ({ container, getByText } = render(<Main {...props} />));
  });

  afterEach(cleanup);

  test("error message is displayed", () => {
    expect(container).toBeDefined();
    expect(getByText("Unable to retrieve weather data.")).toBeDefined();
  });
});
