import React from "react";
import "jest-dom/extend-expect";
import { render, cleanup, fireEvent } from "react-testing-library";
import Form from "../containers/Form";

describe("On initial load", () => {
  let container, getByLabelText, formChange, props;

  formChange = jest.fn();

  beforeEach(() => {
    props = {
      units: "metric",
      formChange: formChange,
      title: "Test form",
      windSwitch: true,
    };

    ({ container, getByLabelText } = render(<Form {...props} />));
  });

  afterEach(cleanup);

  test("form is correctly rendered", () => {
    expect(container).toBeDefined();
    expect(getByLabelText("Title")).toBeDefined();
    expect(getByLabelText("C")).toBeDefined();
    expect(getByLabelText("F")).toBeDefined();
    expect(getByLabelText("On")).toBeDefined();
    expect(getByLabelText("Off")).toBeDefined();
  });

  test("switch defaults are set correctly", () => {
    expect(getByLabelText("C").checked).toBeTruthy();
    expect(getByLabelText("F").checked).toBeFalsy();
    expect(getByLabelText("On").checked).toBeTruthy();
    expect(getByLabelText("Off").checked).toBeFalsy();
  });

  test("formChange is triggered when title field is updated", () => {
    const titleInput = getByLabelText("Title");
    fireEvent.change(titleInput, {
      target: { value: "New title" },
    });
    expect(formChange).toHaveBeenCalled();
    expect(titleInput.value.length).toEqual(9);
  });

  test("formChange is triggered when temperature switch is updated", () => {
    const tempRad = getByLabelText("F");
    fireEvent.click(tempRad, {
      target: { value: "true" },
    });
    expect(formChange).toHaveBeenCalled();
    fireEvent.click(tempRad, {
      target: { value: "false" },
    });
    expect(formChange).toHaveBeenCalled();
  });

  test("formChange is triggered when wind switch is updated", () => {
    const windRad = getByLabelText("Off");
    fireEvent.click(windRad, {
      target: { value: "true" },
    });
    expect(formChange).toHaveBeenCalled();
    fireEvent.click(windRad, {
      target: { value: "false" },
    });
    expect(formChange).toHaveBeenCalled();
  });
});
