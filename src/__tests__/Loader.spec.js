import React from 'react';
import 'jest-dom/extend-expect';
import { render, cleanup } from 'react-testing-library';
import Loader from '../components/Loader';

describe('On initial load', () => {

  let container, props;

  afterEach(cleanup);

  test('loader gets rendered correctly', () => {
    props = {
        display: true,
    };
    ({ container } = render(
        <Loader {...props} />
    ));
    const loader = container.querySelector('.ww-loader');
    expect(loader).toBeTruthy();
    expect(loader).not.toHaveClass('ww-loader__hidden');
  });

  test('loader gets rendered correctly with hidden class when turned off', () => {
    props = {
        display: false,
    };
    ({ container } = render(
        <Loader {...props} />
    ));
    const loader = container.querySelector('.ww-loader');
    expect(loader).toBeTruthy();
    expect(loader).toHaveClass('ww-loader__hidden');
  });
});