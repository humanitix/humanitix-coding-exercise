import React, { Component } from "react";
import Widget from "./containers/Widget";
import Loader from "./components/Loader";
import { getLocation, processLocation, measurementChange } from "./App";
import { css } from "@emotion/css";

const display = css`
  width: 100vw;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: "Sydney",
      dispTemp: 0,
      error: false,
      loading: true,
      weather: {},
      wind: "",
      temp: 0,
    };
  }

  componentDidMount() {
    getLocation((x) => {
      processLocation(x, (o) => {
        this.setState({
          ...o,
          dispTemp: measurementChange(this.props.units, o.dispTemp),
        });
      });
    });
  }

  render() {
    return (
      <div className={display}>
        {this.state.loading ? (
          <Loader display={true} />
        ) : (
          <Widget {...{ ...this.props, ...this.state }} />
        )}
      </div>
    );
  }
}

export default Page;
