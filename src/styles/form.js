import { css } from "@emotion/css";

export const formStyle = css`
  height: 220px;
  margin: 0 64px 0 0;
  width: 40%;
  float: left;

  &__fieldset {
    border: 0 none;
    margin: 0;
    padding: 0;
  }

  &__content {
    width: 100%;
  }

  &__label {
    display: block;
    margin: 0 0 14px;
  }

  &__input[type="text"] {
    border: 1px solid #969696;
    border-radius: 5px;
    font-size: 14px;
    margin: 0 0 18px;
    outline: 0;
    padding: 12px 16px;
    width: 100%;
  }
`;
