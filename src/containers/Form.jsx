import React from "react";
import Input from "../components/Input";
import Radio from "../components/Radio";
import { formStyle } from "../styles/form";

const Form = ({ units, formChange, formSave, title, windSwitch }) => (
  <form
    className={formStyle}
    action="#"
    onSubmit={(e) => {
      e.preventDefault();
      formSave(e);
    }}
  >
    <fieldset className={`${formStyle}__fieldset`}>
      <Input
        label="Title"
        name="title"
        onChange={(e) => formChange(e.target.value.substring(0, 30), "title")}
        placeholder="Title of widget"
        type="text"
        value={title}
      />
      <Radio
        className="ww-temps"
        label="Temperature Unit"
        name="units"
        onChange={(e) => formChange(e.target.value, "units")}
        options={[
          {
            id: "metric",
            label: "°C",
            value: "metric",
          },
          {
            id: "imperial",
            label: "°F",
            value: "imperial",
          },
        ]}
        value={units}
      />
      <Radio
        label="Wind"
        name="windSwitch"
        onChange={(e) =>
          formChange(e.target.value === "true" ? true : false, "windSwitch")
        }
        options={[
          {
            id: "on",
            label: "On",
            value: true,
          },
          {
            id: "off",
            label: "Off",
            value: false,
          },
        ]}
        value={windSwitch}
      />
      <button type="submit">Save</button>
    </fieldset>
  </form>
);

export default Form;
