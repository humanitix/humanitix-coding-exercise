import React from "react";
import WidgetListItem from "./WidgetListItem";
import { css } from "@emotion/css";

const container = css`
  clear: both;
  margin-top: 30px;
`;

const widgetList = css`
  background: #f5f5f5;
  border: 1px solid #969696;
  border-radius: 0 0 10px 10px;
  display: block;
  color: #2a2a2a;
  min-width: 220px;
  overflow: hidden;
  padding: 30px;
  position: relative;
`;

const divider = css`
  background: #1a76d2;
  display: block;
  height: 5px;
  margin: 0 0 10px;
`;

const WidgetList = ({ widgets }) => {
  return (
    <div className={container}>
      <div className={divider} />

      <div className={widgetList}>
        {!widgets || !widgets.length ? (
          <p>No saved widgets</p>
        ) : (
          widgets.map((widget) => (
            <WidgetListItem widget={widget} key={widget._id} />
          ))
        )}
      </div>
    </div>
  );
};

export default WidgetList;
