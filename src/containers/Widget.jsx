import React from "react";
import { css } from "@emotion/css";

const widget = css`
  background: #fff;
  border-radius: 3px;
  margin: 0 auto;
  padding: 32px;
  width: 100%;

  &__title {
    font-size: 14px;
    margin: 0;
    text-transform: uppercase;
  }

  @media screen and (min-width: 48rem) {
    box-shadow: 0px 4px 10px 4px #d2d2d2;
    float: left;
    height: 160px;
    margin: 0 0 0 32px;
    width: 240px;

    &__title {
      margin: 0 0 50px;
    }
  }
`;

const weatherStyle = css`
  text-align: center;

  &__icon {
    display: block;
    margin: 0 auto;
    width: 90px;
  }

  &__city {
    line-height: 1rem;
    margin: 0;
  }

  &__temp {
    display: inline-block;
    font-size: 40px;
    font-weight: 800;
    line-height: 1rem;
    margin: 16px 0;
    position: relative;

    &::after {
      content: "o";
      display: inline-block;
      font-size: 16px;
      position: absolute;
      right: -10px;
      top: -10px;
    }
  }

  &__wind {
    font-size: 12px;
    margin: 0;
  }

  @media screen and (min-width: 48rem) {
    overflow: hidden;
    text-align: left;

    &__icon {
      float: left;
      margin: 0px 24px 0 0;
    }

    &__details {
      display: block;
      float: left;
      width: 106px;
    }
  }
`;

const Widget = ({ city, dispTemp, title, weather, wind, windSwitch }) => (
  <div className={widget}>
    <h1 className={`${widget}__title`}>
      {title.length > 0 ? title : "Title of widget"}
    </h1>
    <div className={weatherStyle}>
      {weather.icon && (
        <img
          alt={weather.description}
          className={`${weatherStyle}__icon`}
          src={`https://openweathermap.org/img/w/${weather.icon}.png`}
        />
      )}
      <div className={`${weatherStyle}__details`}>
        <p className={`${weatherStyle}__city`}>{city}</p>
        <p className={`${weatherStyle}__temp`}>{dispTemp}</p>
        {windSwitch && (
          <p className={`${weatherStyle}__wind`}>
            <strong>Wind</strong> {wind}
          </p>
        )}
      </div>
    </div>
  </div>
);

export default Widget;
