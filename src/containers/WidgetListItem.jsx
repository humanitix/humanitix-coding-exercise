import React from "react";
import { css } from "@emotion/css";

const widgetListItem = css`
  a {
    display: block;
    padding: 12px 16px;
    color: #1a76d2;
  }
`;

const WidgetListItem = ({ widget }) => (
  <div className={widgetListItem}>
    <a href={`/widgets/${widget.slug}`}>
      {widget.title} ({widget.units}, wind: {widget.windSwitch ? "on" : "off"})
    </a>
  </div>
);

export default WidgetListItem;
