import React, { Fragment } from "react";
import Form from "./Form";
import Widget from "./Widget";
import WidgetList from "./WidgetList";
import Loader from "../components/Loader";
import { css } from "@emotion/css";

const content = css`
  background: #F5F5F5;
  border: 1px solid #969696;
  border-radius: 0 0 10px 10px;
  display: block;
  color: #2A2A2A;
  min-width: 220px;
  overflow: hidden;
  padding: 30px;
  position: relative;

  @media screen and (min-width: 48rem) {
      padding: 60px;
  }
`

const divider = css`
  background: #969696;
  height: 1px;
  margin: 20px auto;
  width: 188px;

  @media screen and (min-width: 48rem) {
      height: 188px;
      float: left;
      margin: 16px 0;
      width: 1px;
  }
`

const Main = ({
  city,
  error,
  formChange,
  formSave,
  loading,
  title,
  dispTemp,
  units,
  weather,
  widgets,
  wind,
  windSwitch,
}) => (
  <>
    <div className={content}>
      {error ? (
        <p>Unable to retrieve weather data.</p>
      ) : (
        <Fragment>
          <Loader display={loading} />
          <Form
            units={units}
            formChange={formChange}
            formSave={formSave}
            title={title}
            windSwitch={windSwitch}
          />
          <div className={divider} />
          <Widget
            city={city}
            dispTemp={dispTemp}
            title={title}
            units={units}
            weather={weather}
            wind={wind}
            windSwitch={windSwitch}
          />
        </Fragment>
      )}
    </div>

    <WidgetList widgets={widgets}></WidgetList>
  </>
);

export default Main;
