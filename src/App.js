import React, { Component } from "react";
import Main from "./containers/Main";
import { GetApiHost } from "./configuration/api";
import windDirections from "./configuration/windDirection";
import { css } from "@emotion/css";

const divider = css`
  background: #1a76d2;
  display: block;
  height: 5px;
  margin: 0 0 10px;
`;

const wrapper = css`
  margin: 20px auto;
  max-width: calc(100% - 100px);
  width: 1000px;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
`;

const API_HOST = GetApiHost();

export const testDirection = (deg, dir) => {
  const min = windDirections[dir].min;
  const max = windDirections[dir].max;
  return deg >= min && deg < max;
};

export const fetchWidgets = (cb) => {
  fetch(`${API_HOST}/widgets`).then((res) => res.json().then(cb));
};

export const getWindDirection = (deg) => {
  for (var key in windDirections) {
    if (testDirection(deg, key)) return windDirections[key].dis;
  }
};

export const convertToFahrenheit = (temp) => Math.round((temp * 9) / 5 + 32);

export const measurementChange = (units, temp) => {
  if (units === "metric") return temp;
  return convertToFahrenheit(temp);
};

export const getLocation = (cb, nav = navigator) => {
  const dispLocation = (location) =>
    cb({
      lat: location.coords.latitude,
      lon: location.coords.longitude,
    });

  if (nav.geolocation) return nav.geolocation.getCurrentPosition(dispLocation);
};

export const fetchURL = (location) =>
  `https://api.openweathermap.org/data/2.5/weather?lat=${location.lat}&lon=${location.lon}&APPID=d547cd544b44226d45c92f8436d2eda1&units=metric`;

export const fetchWeather = async (location, success, fail) =>
  await await fetch(fetchURL(location))
    .then((res) => res.json().then((x) => success(x)))
    .catch(() => fail());

export const setWeatherData = (res, cb) => {
  if (res.cod === 200) {
    const readTemp = Math.round(res.main.temp);
    const windDeg = res.wind.deg;
    const windDir = getWindDirection(windDeg);
    const readWind = `${windDir} ${res.wind.speed}km/h`;

    return cb({
      city: res.name,
      dispTemp: readTemp,
      loading: false,
      temp: readTemp,
      weather: res.weather[0],
      wind: readWind,
    });
  } else {
    return cb({
      error: true,
      loading: false,
    });
  }
};

export const processLocation = (location, cb) => {
  if (location)
    return fetchWeather(
      location,
      (x) => setWeatherData(x, cb),
      (x) =>
        cb({
          error: true,
          loading: false,
        })
    );
  else
    return cb({
      error: true,
      loading: false,
    });
};

export const formChange = (value, key, _comp) => {
  const newValues = {
    [key]: value,
  };

  if (key === "units")
    Object.assign(newValues, {
      dispTemp: measurementChange(value, _comp.state.temp),
    });

  return _comp.setState(() => newValues);
};

export const formSave = (e) => {
  const formData = new FormData(e.target);
  const widgetData = Array.from(formData.entries()).reduce(
    (data, entry) => ({
      ...data,
      [entry[0]]: entry[1],
    }),
    {}
  );
  fetch(`${API_HOST}/widgets`, {
    method: "PUT",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(widgetData),
  });
};

class WeatherWidget extends Component {
  constructor(props) {
    super(props);
    this.state = {
      celsius: true,
      city: "Sydney",
      dispTemp: 0,
      error: false,
      loading: true,
      title: "",
      weather: {},
      wind: "",
      windSwitch: true,
      temp: 0,
      widgets: [],
    };
  }

  componentDidMount() {
    getLocation((x) => processLocation(x, (o) => this.setState(() => o)));
    fetchWidgets((widgets) => this.setState({ widgets }));
  }

  render() {
    return (
      <div className={wrapper}>
        <div className={divider} />
        <Main
          formChange={(value, key) => formChange(value, key, this)}
          formSave={formSave}
          {...this.state}
        />
      </div>
    );
  }
}

export default WeatherWidget;
