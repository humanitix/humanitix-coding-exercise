const windDirection = {
  N: {
    dis: "N",
    min: 0,
    max: 11.25,
  },
  NNE: {
    dis: "NNE",
    min: 11.25,
    max: 33.75,
  },
  NE: {
    dis: "NE",
    min: 33.75,
    max: 56.25,
  },
  ENE: {
    dis: "ENE",
    min: 56.25,
    max: 78.75,
  },
  E: {
    dis: "E",
    min: 78.75,
    max: 101.25,
  },
  ESE: {
    dis: "ESE",
    min: 101.25,
    max: 123.75,
  },
  SE: {
    dis: "SE",
    min: 123.75,
    max: 146.24,
  },
  SSE: {
    dis: "SSE",
    min: 146.25,
    max: 168.74,
  },
  S: {
    dis: "S",
    min: 168.75,
    max: 191.25,
  },
  SSW: {
    dis: "SSW",
    min: 191.25,
    max: 213.75,
  },
  SW: {
    dis: "SW",
    min: 213.75,
    max: 236.24,
  },
  WSW: {
    dis: "WSW",
    min: 236.25,
    max: 258.75,
  },
  W: {
    dis: "W",
    min: 258.75,
    max: 281.25,
  },
  WNW: {
    dis: "WNW",
    min: 281.25,
    max: 303.75,
  },
  NW: {
    dis: "NW",
    min: 303.75,
    max: 326.25,
  },
  NNW: {
    dis: "NNW",
    min: 326.25,
    max: 348.75,
  },
  N2: {
    dis: "N",
    min: 348.75,
    max: 361,
  },
};
export default windDirection;
