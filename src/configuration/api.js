const FE_DEV_SERVER_HOST = "localhost:3030";
const API_DEV_SERVER_HOST = "http://localhost:3000";
export const GetApiHost = (win = window) => {
  if (win && win.location && window.location.host === FE_DEV_SERVER_HOST) {
    return API_DEV_SERVER_HOST;
  }
  return "";
};
