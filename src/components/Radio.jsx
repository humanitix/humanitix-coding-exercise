import { css } from "@emotion/css";
import React from "react";
import { formStyle } from "../styles/form";

const radioStyle = css`
  display: block;
  float: left;
  margin: -8px 0 22px;
  width: 50%;
`;

export const renderOptions = (currentValue, name, onChange, options) =>
  options.map(({ id, label, value }) => (
    <div className={radioStyle} key={`frag${id}`}>
      <input
        checked={value === currentValue}
        id={id}
        name={name}
        onChange={onChange}
        value={value}
        type="radio"
      />
      <label className={`${radioStyle}__label`} htmlFor={id}>
        {label}
      </label>
    </div>
  ));

const Radio = ({ className, label, name, onChange, options, value }) => (
  <div className={`${formStyle}__content ${className}`}>
    <p className={`${radioStyle}__label`}>{label}</p>
    {renderOptions(value, name, onChange, options)}
  </div>
);

export default Radio;
