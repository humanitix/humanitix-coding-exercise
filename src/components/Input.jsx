import React from "react";
import { formStyle } from "../styles/form";

const Input = ({ label, name, onChange, placeholder, type, value }) => (
  <div className={`${formStyle}__content`}>
    <label className={`${formStyle}__label`} htmlFor={`ww${name}`}>
      {label}
    </label>
    <input
      className={`${formStyle}__input`}
      id={`ww${name}`}
      name={name}
      onChange={onChange}
      placeholder={placeholder}
      type={type}
      value={value}
    />
  </div>
);

export default Input;
