# Humanitix Coding Exercise 💪🧠🤳

👋 *Hi there non-developer types. If you're interviewing for a QA role, you'll want to go read the [TESTME.MD](https://bitbucket.org/humanitix/humanitix-coding-exercise/src/master/TESTME.MD) intead.*

This exercise is just a way to help us get a better idea of your coding chops without subjecting you to multiple hours of homework, or `deity` forbid, writing code on a whiteboard. It's written with the primary technologies we use at Humanitix (Javascript, Typescript React, and Mongo) but it's not intended to be "perfect code", if such a thing even exists. In fact, that's the point. We want you to help improve it. Here is how it works:

During your technical interview we'll be asking you questions about the code, so have a good look around. You will be asked to run the project, and possibly even write some code. If you'd like to do that on your **own laptop** it'd probably be a good idea to clone it and get it running before the interview. Otherwise you can use one of our laptops on the day.

We'll be acting like it's a normal day on the job, and we're asking for your help to fix a bug, implement a feature, and/or review the code. The exact tasks will depend on what role you're interviewing for, and there is no fixed outcome for the exercise. We really just want to get you talking about, and interacting with code. You can Google whatever you want, you can ask as many questions as you can think of, and you can even wear birkenstocks if you feel the need.

### Project Structure

You'll notice there are three projects in the repo: `database`, `server` and `src`. All three have an `npm run start-dev` command that will start a hot-reloading server for local development.

- `/database` runs an instance of Mongodb in memory. If you stop this, any data you've stored will be lost. `npm run start` this first.
- `/server` is a NodeJs server that connects to the `database`, _serves_ the frontend React app, and acts a REST API. `npm run start` will run it without hot-reloading.
- `/src` is a React app that you can serve via a Webpack dev-server (with `npm run start-dev`). There is no `npm run start` command though. You should `npm run build` which copies the bundle into `server/build/frontend`, and load the site via the `server` URL.

---

### Main page / Widget Editor

![Main page / Widget Editor](./readme/editor.jpg)

### Widget Viewer

![Widget Viewer](./readme/widget.jpg)
